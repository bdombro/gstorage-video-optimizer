# 🔥 gstorage-video-optimizer 🔥

## About
This cli uses ffmpeg to optimize videos in google cloud storage for web. It generates and uploads a .mp4 and .webm file to the same bucket.

## Caveats
1. Though optimized, ffmpeg is hugely CPU hungry
1. This app is CPU single-threaded 

## Requirements
1. Requires Node v8.x or higher

## Installation
```
npm install -g gstorage-video-optimizer
or
yarn global add gstorage-video-optimizer
```

## Usage
```
gstorage-video-optimizer bucket path [options]
    
Flags
  --shutdown, -s shutdown on finish. Useful for auto-scaling

Examples
  $ full-page-cache-warmer gs://get-candeo.appspot.com video_files/wbbpyjgTudQY9QNLCAl7
...
```

## Roadmap

##### Ideas
1. Support GPU acceleration (may already work but haven't verified)

##### Queued
1. None right now

##### In-Process
1. Upkeep, bugfixes and maintenance

## Contributing
Star me! And/or open up a pull request or email me for ideas (see package.json for email)

## Related
1. TBD


## License (MIT)

```
WWWWWW||WWWWWW
 W W W||W W W
      ||
    ( OO )__________
     /  |           \
    /o o|    MIT     \
    \___/||_||__||_|| *
         || ||  || ||
        _||_|| _||_||
       (__|__|(__|__|
```

Copyright (c) 2017 Brian Dombrowski <bdombro@gmail.com>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the 'Software'), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
